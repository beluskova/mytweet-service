package models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import play.db.jpa.GenericModel;
import play.db.jpa.Model;

@Entity
public class Tweeter extends GenericModel
{
  @Id
  public String id;
  public String firstName;
  public String lastName;
  public String email;
  public String password;

  @OneToMany(mappedBy="tweeter", cascade = CascadeType.ALL)
  public List<Tweet> tweets = new ArrayList<Tweet>();
  

  @OneToMany(mappedBy = "sourceUser")
  public List<Friendship> friendships = new ArrayList<Friendship>();

   public Tweeter(String firstName, String lastName, String email, String password)
  {
	//this.id = id;
	this.id = UUID.randomUUID().toString();
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.password = password;
  } 
  
  public static Tweeter findByEmail(String email)
  {
    return find("email", email).first();
  }

  public boolean checkPassword(String password)
  {
    return this.password.equals(password);
  }

  public String getFullName()
  {
    return firstName + " " + lastName;
  }
  
  
  public void befriend(Tweeter friend)
  {
    Friendship friendship = new Friendship(this, friend);
    friendships.add(friendship);
    friendship.save();
    save();
  }

  public void unfriend(Tweeter friend)
  {
    Friendship thisFriendship = null;

    for (Friendship friendship : friendships)
    {
      if (friendship.targetUser == friend)
      {
        thisFriendship = friendship;
      }
    }

    friendships.remove(thisFriendship);
    thisFriendship.delete();
    save();
  }
}
