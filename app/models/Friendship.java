package models;

import javax.persistence.*;
import play.db.jpa.Model;

@Entity
public class Friendship extends Model
{
  @ManyToOne()
  public Tweeter sourceUser;

  @ManyToOne()
  public Tweeter targetUser;

  public Friendship(Tweeter source, Tweeter target)
  {
    sourceUser = source;
    targetUser = target;
  }
}