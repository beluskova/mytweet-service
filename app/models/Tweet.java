package models;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import play.db.jpa.GenericModel;

@Entity
public class Tweet extends GenericModel {
	@Id
	public String id;
	public String editStatus;
	public Date datestamp;

	@ManyToOne
	public Tweeter tweeter;

	public Tweet(String editStatus)
	//, String count, String datestamp)
	{
		this.id = UUID.randomUUID().toString();
		this.editStatus = editStatus;
		this.datestamp = new Date();
	}
	
	 /**
	   * Formats the datestamp field by first converting
	   * to a Date object and then using the private
	   * helper method formattedDate.
	   */
	  public String getDateString()
	  {
	    return formattedDate(new Date());
	  }

	  /**
	   * @param date A java.util.Date object
	   * @see <a href="http://docs.oracle.com/javase/tutorial/i18n/format/simpleDateFormat.html">Date Format</a>
	   * @see <a href="http://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html">Simple Date Format</a>
	   * @return A formatted date string
	   */
	  private String formattedDate(Date date)
	  {
	    DateFormat sdf = new SimpleDateFormat("EEE d MMM yyyy H:mm");
	    sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
	    return sdf.format(datestamp);
	  }
}