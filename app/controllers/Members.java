package controllers;

import play.*;

import java.util.*;

import models.Friendship;
import models.Tweeter;
import play.mvc.*;

public class Members extends Controller {

	public static void index() {
		String userId = session.get("logged_in_userid");
		if (userId != null) {
			Tweeter me = Tweeter.findById(userId);
			List<Tweeter> tweeters = Tweeter.findAll();
			tweeters.remove(me);
			render(tweeters);
		} else {
			Accounts.index();
		}
	}

	public static void friends() {
		String userId = session.get("logged_in_userid");
		if (userId != null) {
			Tweeter tweeter = Tweeter.findById(userId);
			render("Friends/index.html", tweeter);

		} else {
			Accounts.index();
		}
	}

	public static void follow(String id) {
		Tweeter friend = Tweeter.findById(id);
		String userId = session.get("logged_in_userid");
		Tweeter me = Tweeter.findById(userId);
		for (int i = 0; i < me.friendships.size(); i++) {
			if (me.friendships.get(i).targetUser == friend) {
				Logger.info("Trying to befriend " + friend.firstName + " who is already " + me.firstName + "'s friend");
				TweetController.index();
				break;
			}
		}
		me.befriend(friend);
		Logger.info("Following " + friend.firstName);
		me.save();
		TweetController.index();
	}

	public static void drop(String id) {
		String userId = session.get("logged_in_userid");
		Tweeter user = Tweeter.findById(userId);
		Tweeter friend = Tweeter.findById(id);
		user.unfriend(friend);
		Logger.info("Dropping " + friend.email);
		friends();

	}
}