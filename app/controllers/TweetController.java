package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;

public class TweetController extends Controller {
	public static void index() {
		Tweeter tweeter = Accounts.getCurrentUser();
		Logger.info("Tweet ctrler : user is " + tweeter.email);
		List<Tweet> tweets  = Tweet.findAll();
		render("Timeline/index.html", tweets);
	}

	public static void mytweets() {
		Tweeter tweeter = Accounts.getCurrentUser();
		Logger.info("Tweet ctrler : user is " + tweeter.email);
		List<Tweet> tweets = tweeter.tweets;
		render("Timeline/mytweets.html", tweeter, tweets);
	}

	public static void sendTweet() {
		Tweeter tweeter = Accounts.getCurrentUser();
		Logger.info("Tweet ctrler : user is " + tweeter.email);
		render("TweetController/index.html", tweeter);
	}

	public static void newTweet(String message) {
		Tweeter tweeter = Accounts.getCurrentUser();
		String userId = session.get("logged_in_userid");
		Tweet tweet = new Tweet(message);
		tweet.tweeter = tweeter;
		tweet.save();
		index();
	}
}
