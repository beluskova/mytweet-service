package controllers;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import models.Tweet;
import models.Tweeter;
import models.Tweet;
import play.mvc.Controller;
import utils.GsonStrategy;

public class TweetsAPI extends Controller
{
	static Gson gson = new GsonBuilder()
	    .setExclusionStrategies(new GsonStrategy())
	    .create();

  /**
   * 
   * @param id : The tweeter id (tweet owner)
   * @param body : The tweet to be persisted
   */
	  public static void createTweet(String id, JsonElement body)
	  {
		Tweeter tweeter = Tweeter.findById(id);	
	    Tweet tweet = gson.fromJson(body.toString(), Tweet.class);
	    tweeter.tweets.add(tweet);
	    tweet.tweeter = tweeter;
	    tweet.save();
	    renderJSON(gson.toJson(tweet)); 
	  }

	  public static void getAllTweets()
	  {
	    List<Tweet> tweets = Tweet.findAll();
	    renderJSON(gson.toJson(tweets));
	  }

	  /**
	   * 
	   * @param id : The id of the tweeter (the tweet list owner)
	   */
	  public static void getTweets(String id)
	  {
	    Tweeter tweeter = Tweeter.findById(id);
	    if (tweeter == null)
	    {
	      notFound();
	    }
	    renderJSON(gson.toJson(tweeter.tweets));
	  }

	  /**
	   * 
	   * @param id : The tweeter id. This is redundant here since 
	   *             tweet id a uuid and so unique.
	   * @param tweetId : The id of tweet sought.
	   */
	  public static void getTweet (String id, String tweetId)
	  {
	   Tweet tweet = Tweet.findById(tweetId);
	   if (tweet == null)
	   {
	     notFound();
	   }
	   else
	   {
	     renderJSON(gson.toJson(tweet));
	   }
	  }
	  /**
	   * 
	   * @param id : The tweeter id. This is redundant here since 
	   *             tweet id a uuid and so unique.
	   * @param tweetId : The id of tweet for deletion.
	   */ 
	  public static void deleteTweet(String id, String tweetId)
	  {
	    Tweet tweet = Tweet.findById(tweetId);
	    if (tweet == null)
	    {
	      notFound();
	    }
	    else
	    {
	      tweet.delete();
	      renderText("success");
	    }
	  }

	  public static void deleteAllTweets()
	  {
	    Tweet.deleteAll();
	    renderText("success");
	  }  
  }
